import { AppPage } from './app.po';

describe('locations-near-me App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to What Is Near Me?!');
  });
});
