import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { LocationData } from '../shared/LocationData';
import { Results } from '../shared/Results';
import { _ } from 'lodash';

@Injectable()
export class LocationService {

  _dataServiceUrl = 'https://acu-test.herokuapp.com/';

  constructor(private _http: HttpClient) { }

  // Move these declarations to the locationtype and location components
  public allLocations: LocationData;
  public distinctLocationTypes: string[];

  public getData(): any {
    return this._http
      .get<LocationData>(this._dataServiceUrl);
  }

  // Move this to the locationtype component
  public getDistinctLocationTypes(): void {
    this.distinctLocationTypes = _.uniq(_.flatten(_.map(this.allLocations, function(o) { return o.types; }))).sort();
  }

  // Move this to the location component
  public getLocationsByType(locationType: string): Results[] {
    return _.filter(this.allLocations, function(o) { return o.types.includes(locationType); });
  }

}
