import { Component, OnInit } from '@angular/core';
import { LocationService } from '../services/location.service';

import { TitleCasePipe } from '@angular/common';
import { _ } from 'lodash';
import { LocationData } from '../shared/LocationData';
import { Observable } from 'rxjs/observable';

@Component({
  selector: 'app-locationtype',
  templateUrl: './locationtype.component.html',
  styleUrls: ['./locationtype.component.css']
})
export class LocationtypeComponent implements OnInit {

  // These should come from the service...
  distinctLocationTypes: string[] = [
    'bar',
    'establishment',
    'food',
    'lodging',
    'meal_delivery',
    'meal_takeaway',
    'point_of_interest',
    'restaurant',
    'store'
  ];
  selectedLocationType: string;

  constructor(private _locationService: LocationService) { }

  ngOnInit() {
    // Get data from location service, then strip out distinct location types
    // then store it in a local variable for quick updates
    // this.distinctLocationTypes =
    //   _.uniq(
    //     _.flatten(
    //       _.map(
    //         // this.allLocations,
    //         function(o) { return o.types; }))).sort();
  }

  onSelect(locationType) {
    console.log(locationType);
    this.selectedLocationType = locationType;
  }

}
