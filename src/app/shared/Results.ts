import { Geometry } from './Geometry';
import { OpeningHours } from './OpeningHours';
import { Photo } from './Photo';

export interface Results {
  geometry: Geometry;
  icon: string;
  id: string;
  name: string;
  openingHours: OpeningHours;
  photos: Photo;
  placeId: string;
  rating: number;
  reference: string;
  scope: string;
  types: string[];
  vicinity: string;
}
