import { Results } from './Results';

export class LocationData {
  html_attributions: string;
  results: Results[];
  status: string;
}
