import { Viewport } from './Viewport';

export class Geometry {
  location: Location;
  viewport: Viewport;
}
