import { ReplaceUnderscoresWithSpacesPipe } from './replace-underscores-with-spaces.pipe';

describe('ReplaceUnderscoresWithSpacesPipe', () => {
  it('create an instance', () => {
    const pipe = new ReplaceUnderscoresWithSpacesPipe();
    expect(pipe).toBeTruthy();
  });
});

describe('ReplaceUnderscoresWithSpacesPipe', () => {
  it('has some underscores', () => {
    const pipe = new ReplaceUnderscoresWithSpacesPipe();
    const expected = 'point of interest';
    const actual = pipe.transform('point_of_interest');
    expect(expected).toBe(actual);
  });
});

