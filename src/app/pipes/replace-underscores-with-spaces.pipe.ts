import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceUnderscoresWithSpaces'
})
export class ReplaceUnderscoresWithSpacesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(/_/g, ' ');
  }

}
