import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LocationtypeComponent } from './locationtype/locationtype.component';
import { LocationService } from './services/location.service';
import { LocationComponent } from './location/location.component';
import { LocationDetailsComponent } from './location-details/location-details.component';
import { ReplaceUnderscoresWithSpacesPipe } from './pipes/replace-underscores-with-spaces.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LocationtypeComponent,
    LocationComponent,
    LocationDetailsComponent,
    ReplaceUnderscoresWithSpacesPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [LocationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
