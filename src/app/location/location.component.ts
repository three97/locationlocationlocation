import { Component, OnInit, Input } from '@angular/core';
import { LocationService } from '../services/location.service';
import { LocationData } from '../shared/LocationData';
import { Observable } from 'rxjs/Observable';
import { Results } from '../shared/Results';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  // Accept locationType from locationType component so that we can
  // hydrate the list based on the selected locationType
  @Input() locationType: string;

  selectedLocation: Results;
  locationData: LocationData[];

  constructor(private _locationService: LocationService) { }

  ngOnInit() {
  }

  onSelectedLocation(locationId) {
    // use the locationId to get more information about the location from the service
    // and then pass that value to the location-details component so that it can
    // populate the location details through its markup
  }

  // getLocationData(): void {
  //   this._locationService.getData()
  //     .subscribe(ld => this.locationData = ld);
  // }

}
