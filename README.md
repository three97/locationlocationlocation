# LocationsNearMe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## User Story
Front End User Story

As a user, I want to be able to view location types, location names, and location addresses, so that I can find locations around me.

### Acceptance Criteria:
1. User will see a list of location types in a column on the left of the screen
1. Clicking one of the location types will display a second column with a list of the location names associated with the selected location type
   1. The selected location type will be highlighted to indicate that it has been selected
   2. Clicking a different location type will
      * Update the list of locations
      * Clear the displayed address, as there will no longer be a selected location
1. Clicking one of the locations will display the address for the selected location
   1. The selected location name will be highlighted to indicate that it has been selected
   1. Clicking a different location will update the address displayed
1. All data should come from: https://acu-test.herokuapp.com/ (it is also in src/data/locationData.json)
1. Application should be coded in a FE framework, such as Angular 4+
